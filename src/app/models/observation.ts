// interfaces exactly matching to FHIR Bundle Resource 

export interface Bundle {
    resourceType: string;
    id: string;
    meta: Meta;
    type: string;
    total: number;
    link: Link[];
    entry: Entry[];
  }
  
export interface Entry {
    fullUrl: string;
    resource: Resource;
    search: Search;
  }
  
export interface Search {
    mode: string;
  }
  
export interface Resource {
    resourceType: string;
    id: string;
    meta: Meta2;
    status: string;
    category: Category[];
    code: Code;
    subject: Subject;
    encounter: Subject;
    effectiveDateTime: string;
    issued: string;
    valueCodeableConcept: Code;
  }
  
 export interface Subject {
    reference: string;
  }
  
 export interface Code {
    coding: Coding[];
    text: string;
  }
  
 export interface Category {
    coding: Coding[];
  }
  
 export interface Coding {
    system: string;
    code: string;
    display: string;
  }
  
 export interface Meta2 {
    versionId: string;
    lastUpdated: string;
    source: string;
    profile: string[];
    tag: Tag[];
  }
  
 export interface Tag {
    system: string;
    code: string;
  }
  
 export interface Link {
    relation: string;
    url: string;
  }
  
 export interface Meta {
    lastUpdated: string;
  }