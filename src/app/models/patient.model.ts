// class and interfaces to create a FHIR patient resource. 

export class Patient{
    resourceType?: string;
    id: string;
    name: HumanName;
    gender: string;
    birthDate: string;
    telecom: ContactPoint;

    constructor(){
        this.resourceType = "Patient";
        this.id = "";
        this.name = {} as HumanName;
        this.gender = "";
        this.birthDate = "";
        this.telecom = {} as ContactPoint;
    }
}

export interface HumanName{
    family: string;
    given: string;
}

export interface ContactPoint{
    system: string;
    value: string;
    use: string;
}