import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bundle, Entry } from '../../models/observation';


@Injectable({
  providedIn: 'root'
})
export class CovidService {

  
  private covidApi:string = "https://hapi.fhir.org/baseR4/Observation?code=94531-1&_sort=-date&_count=40";


  constructor(private httpClient: HttpClient) { }


  // get a Bundle resource containing the latest 40 FHIR observation resources 
  //with the code 94531-1 (COVID-19) from HAPI FHIR public server server
  getData():Observable<Bundle>{  
    return this.httpClient.get<Bundle>(this.covidApi);
    
  }
}
 