import { Injectable, OnInit } from '@angular/core';
import { Patient } from '../../models/patient.model';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class PatientService {

  private api:string = "https://hapi.fhir.org/baseR4/Patient/";

  constructor(private httpCleint: HttpClient){

  }

  getPatient(){
    return this.httpCleint.get(this.api);
  }
  //creating and sending a new patient FHIR object to the HAPI FHIR public test server. 
  //If succeeded, fetch the new patient resource and return the id to display the URL
  addPatient(patient:Patient): Observable<Patient>{
    return this.httpCleint.post<Patient>(this.api, patient).pipe(
      tap((newPatient: Patient) => console.log(`resource: ${this.api+newPatient.id}`)),

    );
  }  
  
}