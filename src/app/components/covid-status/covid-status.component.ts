import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CovidService } from '../../services/covid/covid.service';
import { Entry } from '../../models/observation';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-covid-status',
  templateUrl: './covid-status.component.html',
  styleUrls: ['./covid-status.component.css']
})


export class CovidStatusComponent implements OnInit { 
   public entries: Entry[]; // Separating Entry objects from the Bundle object we received from the server. 
   displayedColumns: string[] = ['id','effectiveDateTime','text','status','subject']; //attributes of the display table
   dataSource: MatTableDataSource<Entry>; // declaring the data used for the display table

  constructor(private covidService: CovidService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.getData();
    
    
  }  

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator; // adding pagination feature to the table 

    
   
}

  public getData(){
    this.covidService.getData().subscribe(data => {
      this.entries = data.entry;              // Separating Entry objects from the Bundle object we received from the server. 
      this.dataSource.data = data.entry;
    });
    }
  

}
