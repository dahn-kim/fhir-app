import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { ContactPoint, HumanName, Patient } from 'src/app/models/patient.model';
import { PatientService } from '../../services/patient/patient.service';

import * as _moment from 'moment';

const moment = _moment;

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit{ 
 
  patient: Patient;
  patientForm: FormGroup;

  genderOption = ['male', 'female', 'unknown'];
  telecomOption = ['home', 'work', 'temp', 'mobile'];

  constructor(private fb: FormBuilder, public patientService: PatientService) { }

  msgTrue = false;
  url ="http://hapi.fhir.org/baseR4/Patient/";
  ngOnInit(): void {
    this.initialiseForm();
  }

  //initialising the patient creation form 
  initialiseForm(): void {              
    this.patientForm = this.fb.group({
      given: '',
      family: '',
      telecom: '',
      telecomSelect: '',
      gender: '',
      birthDate: '',
    });
  }

  selectGender(event): void{
    this.patientForm.patchValue({
      gender: event.target.value
    });
  }

  selectTelecomUse(event): void{
    this.patientForm.patchValue({
      telecomSelect: event.target.value
    });}

   onSubmit(): void{
    
    console.log(this.patientForm.value.gender);
    let dob: Date = this.patientForm.value.birthDate;
    let formattedDate =(moment(dob).format('YYYY-MM-DD')); //reforamtting the date of birth to YYYY-MM-DD format without time display
    
    // creating a FHIR patient resource by binding all attributes
    let nameFamily:HumanName = {family: this.patientForm.value.family, given: this.patientForm.value.given};
    const telecomValue:ContactPoint = {system: "phone", value: this.patientForm.value.telecom, use: this.patientForm.value.telecomSelect };
    const newPatient:Patient = { resourceType : "Patient", id: "", name: nameFamily, gender:this.patientForm.value.gender,  birthDate: formattedDate, telecom:telecomValue };
    
    this.patientService.addPatient(newPatient).subscribe(data => {
      console.log(data);
      this.msgTrue = true;
      this.url = data.id; //this will display a URL of newly created patient resource

    })   
  } 
}



