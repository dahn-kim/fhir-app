import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PatientFormComponent } from './components/patient-form/patient-form.component';
import { CovidStatusComponent } from './components/covid-status/covid-status.component';
const routes: Routes = [
  { path: '', redirectTo: '/covid-19', pathMatch: 'full' },
  { path: 'new-patient', component: PatientFormComponent},
  { path: 'covid-19', component: CovidStatusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
