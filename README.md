# FHIR App

This Angular web application lets users enter new patient data and binds the data into a new FHIR Patient resource which is sent to HAPI FHIR PUBLIC TEST R4 server. Also the app displays the most recent COVID-19 testing results from the server.

the app is published in : https://fhir-app.azurewebsites.net/


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 
Right click your browser and check console section. 


# Application Features

The Fhir app was developed based on an imaginary condition that a national health authority has provided a national/regional server network to exchange health data between health providers. To store and retrieve FHIR resources, HAPI FHIR Public test server was used as the project’s data source. 

The application consists of two main features:
1.	Display recent COVID-19 testing results in an imaginary local area for health providers and health authority.

2.	Creating a simple patient profile in a standardized way and store it in a server.

![figure](/readme_fig_1.png)


# Materials and Methods

#### FHIR (Fast Healthcare Interoperable Resources)
FHIR (Fast Healthcare Interoperable Resources) is the new standard to exchange resources in XML and JSON formats. It was developed from Health Level Seven International (HL7) and is based on REST-Architecture. FHIR can be used for example to share data between EHR systems, for the communication between servers of healthcare providers or within mobile applications.

#### HAPI FHIR Public Test Server
To store and retrieve FHIR resources, HAPI FHIR Public test server was used as the project’s data source. The server is a complete implementation of a FHIR server which also stores diverse FHIR resources globally received from http post requests. Therefore, HAPI FHIR public test server was decided to use to retrieve FHIR-standardised artificial COVID-19 test
results and to create and store FHIR patient resources. However, since the focus of the project is to exchange interoperable medical data, a secured server network should be used in real time.

#### LOINC (Logical Observation Identifiers Names and Codes)
LOINC provides internationally standardised terminologies to identify health measurements, observations and documents. As FHIR requires standardised terminologies such as the title of diagnosis or methodologies used from a lab tests, COVID-19 terminology was referenced from LOINC. 

#### Angular framework & Angular Material UI component library
Angular framework was used to build a web application and to handle the http client side to retrieve COVID-19 test result FHIR resources and to create patient FHIR resource via HAPI FHIR public test server. Angular Material UI component library was used to improve the aesthetics of front-end feature of the application.

#### Gitlab & Microsoft Azure DevOps
To maintain the version control of code in track, Gitlab repository was used. For deployment of Fhir app, App Services and DevOps from Microsoft Azure were used.


### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
